﻿using Application.Services.Interfaces;
using Domain.Entities;
using Domain.Interfaces;
using Infrastructure.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Application.Services.Implementations
{
    public class QuestionaireService : IQuestionaireService
    {
        //Container of repositories
        private readonly IUnitOfWork _uow;
        // Constructor receiving Unit of work container which contains all repositories
        public QuestionaireService(IUnitOfWork uow) 
        {
            _uow = uow;
        }
        //summary
        // THis method fetch data from database and returns list of all exist questions
        public IEnumerable<Question> GetAllQuestions()
        {
            return _uow.QuestionRepository.All;
        }
        //summary
        //This method saves all answers comming from client App asynchronously using Answer Repository
        public async Task SaveAnsersAsync(IEnumerable<AnswerViewModel> answers)
        {
            //colelct all results and store to Answer object
            var results = answers.Select(z => new Answer()
            {
                Content = z.Answer,
                QuestionId = z.QuestionId
            });
            //insert results to database
            await _uow.AnswerRepository.AddRangeAsync(results);
            await _uow.SaveAsync();
        }
    }
}
