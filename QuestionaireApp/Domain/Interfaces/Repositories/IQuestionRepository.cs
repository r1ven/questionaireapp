﻿using Domain.Entities;

namespace Domain.Interfaces.Repositories
{
    public interface IQuestionRepository : IRepository<Question, int>
    {
    }
}
