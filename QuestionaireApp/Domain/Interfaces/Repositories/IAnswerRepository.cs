﻿using Domain.Entities;

namespace Domain.Interfaces.Repositories
{
    public interface IAnswerRepository : IRepository<Answer, int>
    {
    }
}
