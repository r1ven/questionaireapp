﻿using Domain.Interfaces;
using Domain.Interfaces.Repositories;
using System.Threading.Tasks;

namespace Infrastructure.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly AppDbContext context;
        private IQuestionRepository questionRepository;
        private IAnswerRepository answerRepository;
        public UnitOfWork(AppDbContext context)
        {
            this.context = context;
        }

        public IQuestionRepository QuestionRepository
        {
            get { return questionRepository ??= new QuestionRepository(context); }
        }
        public IAnswerRepository AnswerRepository
        {
            get { return answerRepository ??= new AnswerRepository(context); }
        }

        public void Save()
        {
            context.SaveChanges();
        }

        public async Task SaveAsync()
        {
            await context.SaveChangesAsync();
        }
    }
}
